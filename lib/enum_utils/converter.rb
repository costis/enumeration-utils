module EnumUtils
  class Converter

    def self.ranges(sequence)
      arr = []
      ranges = []

      sequence.each do |item|
        if arr.empty?
          arr << item 
          next
        end

        diff = item - arr.last
        if diff == 1
          arr << item
        else
          ranges << arr.dup
          arr.clear
          arr << item
        end
        ranges << arr if item == sequence.last
      end

      ranges.map { |r| r.size > 1 ? "#{r.first}:#{r.last}" : r.first }.join(',')
    end

    def self.sequence(ranges)
      ranges.split(",").map do |item|
        item.include?(":") ? Range.new(item.split(":").first.to_i, item.split(":").last.to_i).to_a : item.to_i
      end.flatten
    end

  end
end
