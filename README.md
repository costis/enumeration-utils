Enumerable to ranges
====================
Convert an enumerable of numbers into a string formatted comma-separated list of
ranges, of the format "from:to".

    Example: EnumUtils.Converter.ranges([1,2,3,5]) = "1:3,5"

String to Enumerable
====================
Convert a string with ranges to an enumerable
    Example: EnumUtils.Converter.sequence("1:3,5").to_a=[1,2,3,5]


