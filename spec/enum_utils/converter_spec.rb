require "spec_helper"

module EnumUtils
  describe Converter do
    describe ".ranges" do
      it "should extract ranges from an enumeration - #1" do
        c = EnumUtils::Converter.new
        EnumUtils::Converter.ranges([1,2,3,5]).should eq "1:3,5"
      end

      it "should extract ranges from an enumeration - #2" do
        EnumUtils::Converter.ranges([1,2,3,5,7,8,9]).should eq "1:3,5,7:9"
      end

      it "should extract ranges from an enumeration - #3" do
        EnumUtils::Converter.ranges([1,3,4,5,9,10,12]).should eq "1,3:5,9:10,12"
      end

      it "should extract ranges from an enumeration - #4" do
        EnumUtils::Converter.ranges([1,3,5,7]).should eq "1,3,5,7"
      end
    end

    describe ".sequence" do
      it "should convert string list to enumeration = #1" do
        EnumUtils::Converter.sequence("1:3,5").should eq [1,2,3,5]
      end

      it "should convert string list to enumeration = #2" do
        EnumUtils::Converter.sequence("1:3,5:7").should eq [1,2,3,5,6,7]
      end

      it "should convert string list to enumeration = #3" do
        EnumUtils::Converter.sequence("1,3,5,7").should eq [1,3,5,7]
      end

      it "should convert string list to enumeration = #4" do
        EnumUtils::Converter.sequence("1:3,5,7:9,11").should eq [1,2,3,5,7,8,9,11]
      end
    end
  end
end
